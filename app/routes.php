<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::get('/perfil', array('before' => 'auth.basic', function()
{
	return View::make('perfil');
}));

Route::get('/logout', function()
{
	Auth::logout();
	return Redirect::route('/');
});

Route::get('/{url}', function($url)
{
	return View::make($url);
});
