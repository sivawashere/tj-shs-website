<html lang="es">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		{{ HTML::style('style.css'); }}
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js"></script>
		<title>@yield('title')</title>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="/">TJ SHS</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/resumen">sobre nosotros</a></li>
						<li><a href="/horario">horario</a></li>
						<li><a href="/armario">armario</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="/perfil">mi perfil</a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div class="container">
			@yield('content')
		</div>
	</body>
</html>
