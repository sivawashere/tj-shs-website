@extends('layouts.page')

@section('title')
	Sobre la Sociedad
@stop

@section('content')
	<p>The Spanish Honor Society is dedicated to the appreciation and conservation of the Spanish language and culture that the Northern Virginia community provides. It encourages a continuity of interest in Spanish studies. Its motto is "All Together for One Goal". Students are chosen to become members of the honor society through an application process and are expected to have proficient Spanish language skills in order to truly benefit from the society which correlates with the Society’s mission of high academic achievement. Cultural Spanish events are regularly held by the honor society in order to promote the culture and create true realization of the beauty of the language.</p>
@stop
