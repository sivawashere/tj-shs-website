<?php

class UserSeeder extends DatabaseSeeder {
	public function run() {
		$users = [[
			'firstname' => 'Siva',
			'lastname'  => 'Somayyajula',
			'email'     => 'siva.somayyajula@gmail.com',
			'password'  => Hash::make('test')
		]];
		foreach ($users as $user) {
			User::create($user);
		}
	}
}
